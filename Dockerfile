FROM nginx:alpine
LABEL author="Oladimeji Ajeniya"
COPY ./dist /user/share/nginx/html
EXPOSE 80 443
ENTRYPOINT ["NGINX", "-G", "DEAMON OFF;"]
